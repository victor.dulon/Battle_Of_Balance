extends Node2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
var cool_down : float
var force_du_tir : float
@export var adversaire : bool = false

### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _point_origine : Marker2D
var _point_direction : Marker2D
var _balle : PackedScene
var _is_active : bool = false
var _animation_player : AnimationPlayer


### INHERITED METHOD
func _ready():
	_Assignation_De_Variables()
	_Connexion_Signaux()
	if RuntimeData.is_playing == true :
		_is_active = true
	_Initialisation_Du_Tir()
	MiseAJourStatistiques()
	
	pass

func _process(delta : float):
	pass

### PUBLIC METHOD
func MiseAJourStatistiques():
	await get_tree().create_timer(0.1).timeout
	print("Mise à jour statistique !")
	if !adversaire :
		cool_down = RuntimeData.joueur_cool_down
		force_du_tir = RuntimeData.joueur_force_tir
	else :
		cool_down = RuntimeData.adversaire_cool_down
		force_du_tir = RuntimeData.adversaire_force_tir

### PRIVATE METHOD
func _Assignation_De_Variables():
	var _enfants = self.get_children()
	for _enfant in _enfants :
		if _enfant is Marker2D :
			if _enfant.name == "Origine" : _point_origine = _enfant
			else : _point_direction = _enfant
		elif _enfant is AnimationPlayer :
			_animation_player = _enfant
	
	_balle = Bibliotheque.balle_classique_prefab
	
func _Initialisation_Du_Tir():
	if _is_active :
		await get_tree().create_timer(cool_down).timeout
		if _is_active:
			_Tir()
			_Initialisation_Du_Tir()

func _Tir():
	var _instance_de_balle = _balle.instantiate()
	_instance_de_balle.position = _point_origine.position
	var _enfants = _instance_de_balle.get_children()
	var _rigid_body_balle
	for _enfant in _enfants :
		if _enfant is RigidBody2D :
			_rigid_body_balle = _enfant
	var _direction_balle

	_direction_balle = -(_point_origine.global_position - _point_direction.global_position)

	add_child(_instance_de_balle)
	_animation_player.play("Tir")
	_rigid_body_balle.apply_central_force(_direction_balle * force_du_tir)

func _Start_Tir():
	_is_active = true
	_Initialisation_Du_Tir()

func _Stop_Tir():
	_is_active = false


func _Connexion_Signaux():
	EventManager._on_start_game.connect(_Start_Tir)
	EventManager._on_end_game.connect(_Stop_Tir)
	EventManager._on_amelioration_done.connect(MiseAJourStatistiques)
	EventManager._on_achat_upgrade.connect(MiseAJourStatistiques)
