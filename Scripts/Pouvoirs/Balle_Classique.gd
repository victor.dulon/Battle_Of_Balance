extends Node2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE

### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _rigid_body : RigidBody2D

### INHERITED METHOD
func _ready():
	_Assign_Variables()
	_Connect_Signals()
	
	pass

func _process(delta ):
	pass


### SIGNAL METHOD
func _Connect_Signals():
	_rigid_body.body_entered.connect(_on_body_entered)
	EventManager._on_end_game.connect(_on_end_game)

### PUBLIC METHOD


### PRIVATE METHOD
func _Assign_Variables():
	var _enfants = self.get_children()
	for _enfant in _enfants :
		if _enfant is RigidBody2D:
			_rigid_body = _enfant
			_rigid_body.contact_monitor = true

func _on_body_entered(body):
	print(self, " est entré dans ", body)
	var miroir = body.get_parent()
	if miroir.name == "Miroir":
		miroir.Recoit_Degat()

func _on_end_game():
	self.queue_free()
