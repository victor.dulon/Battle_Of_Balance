extends Control


@export var label_gagnant : Label
@export var bouton_recommencer : Button

func _ready():
	_Connexion_Signaux()


func _Connexion_Signaux():
	EventManager._on_player_dead.connect(_on_player_dead)
	bouton_recommencer.pressed.connect(_on_bouton_recommencer_pressed)
	
func _on_bouton_recommencer_pressed():
	self.visible = false
	GameManager.Restart_Game()

func _on_player_dead(player : String):
	self.visible = true
	if player == "Joueur":
		label_gagnant.text = "Joueur 01 a gagné"
	else :
		label_gagnant.text = "Joueur 02 a gagné"
