extends Node2D

var _sprite2D : Sprite2D
var _area2D : Area2D
var _tween : Tween

func _ready():
	EventManager._on_construction_choisie.connect(_Update_Visuel_Preview)
	_Assignation_Variables()
	_area2D.area_entered.connect(_on_area_entered)
	_area2D.area_exited.connect(_on_area_exited)
	EventManager._on_start_game.connect(_on_start)
	EventManager._on_end_game.connect(_on_end)

func _input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_WHEEL_UP:
		Tourne_Piece(true)
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
		Tourne_Piece(false)
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_RIGHT:
		Cache_Preview_Temporairement()

func _physics_process(delta):
	var mouse_position = get_global_mouse_position()
	var position = mouse_position # Vitesse de suivi
	position.y = position.y
	set_position(position)

func _Update_Visuel_Preview(type : String):
	if type == "Mur_Joueur":
		_sprite2D.texture = Bibliotheque.mur_joueur_texture
	elif type == "Mur_Adversaire":
		_sprite2D.texture = Bibliotheque.mur_adversaire_texture
	elif type == "Tireur_Joueur":
		_sprite2D.texture = Bibliotheque.tireur_joueur_texture
	elif type == "Tireur_Adversaire":
		_sprite2D.texture = Bibliotheque.tireur_adversaire_texture
	elif type == "Miroir_Joueur":
		_sprite2D.texture = Bibliotheque.miroir_joueur_texture
	elif type == "Miroir_Adversaire":
		_sprite2D.texture = Bibliotheque.miroir_joueur_texture
	
	_sprite2D.rotation = 0
	_sprite2D.scale = Vector2(0.3,0.3)

func _Assignation_Variables():
	var enfants = self.get_children()
	for enfant in enfants :
		if enfant is Sprite2D:
			_sprite2D = enfant
		elif enfant is Area2D:
			_area2D = enfant

func _on_area_entered(body : Area2D):
	_sprite2D.visible = false

func _on_area_exited(body : Area2D):
	_sprite2D.visible = true

func _on_start():
	self.visible = false

func _on_end():
	self.visible = true

func Tourne_Piece(positif : bool):
	var rotation : int
	if RuntimeData.choix_construction_actuelle == "Mur_Joueur" or RuntimeData.choix_construction_actuelle == "Mur_Adversaire":
		rotation = 45
	elif RuntimeData.choix_construction_actuelle == "Tireur_Joueur" or RuntimeData.choix_construction_actuelle == "Tireur_Adversaire":
		rotation = 45
	elif RuntimeData.choix_construction_actuelle == "Miroir_Joueur" or RuntimeData.choix_construction_actuelle == "Miroir_Adversaire":
		rotation = 10
	
	if !positif : rotation = -rotation
	
	_sprite2D.rotation_degrees += rotation

func Cache_Preview_Temporairement():
	_sprite2D.visible = false
	await get_tree().create_timer(1).timeout
	_sprite2D.visible = true
