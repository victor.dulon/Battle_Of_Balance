extends Control

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var _bouton_mur_joueur : Button
@export var _bouton_tireur_joueur : Button
@export var _bouton_mur_adversaire : Button
@export var _bouton_tireur_adversaire : Button
@export var _bouton_start : Button
@export var _argent_joueur : Label
@export var _argent_adversaire : Label

@export var _bouton_miroir_joueur : Button
@export var _bouton_miroir_adversaire : Button

@export var _bouton_amelioration_mur_joueur : Button
@export var _bouton_amelioration_force_tir_joueur : Button
@export var _bouton_amelioration_cadence_tir_joueur : Button

@export var _bouton_amelioration_mur_adversaire : Button
@export var _bouton_amelioration_force_tir_adversaire : Button
@export var _bouton_amelioration_cadence_tir_adversaire : Button

@export var _label_stat_poids_mur_joueur : Label
@export var _label_stat_force_tir_joueur : Label
@export var _label_stat_cadence_tir_joueur : Label

@export var _label_stat_poids_mur_adversaire : Label
@export var _label_stat_force_tir_adversaire : Label
@export var _label_stat_cadence_tir_adversaire : Label

@export var _timer_label : Label
@export var _control_boutons_joueurs : Control

### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _is_playing : bool = false
var _time_remaining : int = 15
var _timer_on : bool = true

### INHERITED METHOD
func _ready():
	_Connect_Signals()
	MAJ_Argent()
	MAJStats()
	pass

func _process(delta : float):
	pass

### SIGNAL METHOD
func _Connect_Signals():
	_bouton_mur_joueur.pressed.connect(_on_bouton_pressed.bind("Mur_Joueur"))
	_bouton_tireur_joueur.pressed.connect(_on_bouton_pressed.bind("Tireur_Joueur"))
	_bouton_miroir_joueur.pressed.connect(_on_bouton_pressed.bind("Miroir_Joueur"))
	_bouton_mur_adversaire.pressed.connect(_on_bouton_pressed.bind("Mur_Adversaire"))
	_bouton_tireur_adversaire.pressed.connect(_on_bouton_pressed.bind("Tireur_Adversaire"))
	_bouton_miroir_adversaire.pressed.connect(_on_bouton_pressed.bind("Miroir_Adversaire"))
	_bouton_start.pressed.connect(_on_bouton_pressed.bind("Start"))
	EventManager._mise_a_jour_argent.connect(MAJ_Argent)
	EventManager._on_start_game.connect(_MAJ_Control_Boutons.bind("Start"))
	EventManager._on_end_game.connect(_MAJ_Control_Boutons.bind("End"))
	EventManager._on_end_game.connect(_on_end_game)
	EventManager._on_player_dead.connect(Stop_Timer)
	_bouton_amelioration_mur_joueur.pressed.connect(_on_bouton_amelioration_pressed.bind("Joueur", "Mur"))
	_bouton_amelioration_force_tir_joueur.pressed.connect(_on_bouton_amelioration_pressed.bind("Joueur", "Force"))
	_bouton_amelioration_cadence_tir_joueur.pressed.connect(_on_bouton_amelioration_pressed.bind("Joueur", "Cadence"))
	_bouton_amelioration_mur_adversaire.pressed.connect(_on_bouton_amelioration_pressed.bind("Adversaire", "Mur"))
	_bouton_amelioration_force_tir_adversaire.pressed.connect(_on_bouton_amelioration_pressed.bind("Adversaire", "Force"))
	_bouton_amelioration_cadence_tir_adversaire.pressed.connect(_on_bouton_amelioration_pressed.bind("Adversaire", "Cadence"))

### PUBLIC METHOD
func MAJ_Argent():
	_argent_joueur.text = "Argent : " + str(RuntimeData.joueur_money)
	_argent_adversaire.text = "Argent : " + str(RuntimeData.adversaire_money)

func MAJStats():
	_label_stat_poids_mur_joueur.text = "Poids Mur : " + str(RuntimeData.joueur_poids_mur) +"kg"
	_label_stat_force_tir_joueur.text = "Force Tir : " + str(RuntimeData.joueur_force_tir)
	_label_stat_cadence_tir_joueur.text = "Cool Down : " + str(RuntimeData.joueur_cool_down)
	
	_label_stat_poids_mur_adversaire.text = "Poids Mur : " + str(RuntimeData.adversaire_poids_mur) +"kg"
	_label_stat_force_tir_adversaire.text = "Force Tir : " + str(RuntimeData.adversaire_force_tir)
	_label_stat_cadence_tir_adversaire.text = "Cool Down : " + str(RuntimeData.adversaire_cool_down)

### PRIVATE METHOD
func _on_bouton_pressed(type : String):
	if !_is_playing:
		if type == "Mur_Joueur":
			EventManager.InvokeOnConstrutionChoisie(type)
		elif type == "Mur_Adversaire":
			EventManager.InvokeOnConstrutionChoisie(type)
		elif type == "Tireur_Joueur":
			EventManager.InvokeOnConstrutionChoisie(type)
		elif type == "Tireur_Adversaire":
			EventManager.InvokeOnConstrutionChoisie(type)
		elif type == "Miroir_Joueur":
			EventManager.InvokeOnConstrutionChoisie(type)
		elif type == "Miroir_Adversaire":
			EventManager.InvokeOnConstrutionChoisie(type)
		elif !_is_playing:
			_is_playing = true
			_timer_on = true
			_bouton_start.disabled = true
			_Start_Timer()
			EventManager.InvokeOnStartGame()

func _on_bouton_amelioration_pressed(joueur : String, type : String):
	if !_is_playing:
		if joueur == "Joueur":
			if type == "Mur" and RuntimeData.joueur_money >= 50:
				EventManager.InvokeOnArgentDepense(joueur, 50)
				RuntimeData.Changer_Poids_Mur(joueur)

			elif type == "Force" and RuntimeData.joueur_money >= 100 :
				EventManager.InvokeOnArgentDepense(joueur, 100)
				RuntimeData.Changer_Force_Tir(joueur)

			elif type == "Cadence" and RuntimeData.joueur_money >= 100 :
				EventManager.InvokeOnArgentDepense(joueur, 100)
				RuntimeData.Changer_Cool_Down(joueur)
				
		else:
			if type == "Mur" and RuntimeData.adversaire_money >= 50:
				EventManager.InvokeOnArgentDepense(joueur, 50)
				RuntimeData.Changer_Poids_Mur(joueur)

			elif type == "Force" and RuntimeData.adversaire_money >= 100 :
				EventManager.InvokeOnArgentDepense(joueur, 100)
				RuntimeData.Changer_Force_Tir(joueur)

			elif type == "Cadence" and RuntimeData.adversaire_money >= 100 :
				EventManager.InvokeOnArgentDepense(joueur, 100)
				RuntimeData.Changer_Cool_Down(joueur)
				
		EventManager.InvokeOnAchatUpgrade()
		EventManager.InvokeOnMiseAJourArgent()
		MAJStats()

func _Start_Timer():
	if _timer_on:
		await get_tree().create_timer(1).timeout
		if _time_remaining == 0:
			EventManager.InvokeOnEndGame()
			return
		_time_remaining -= 1
		_timer_label.text = str(_time_remaining) + " s"
		_Start_Timer()

func _on_end_game():
	_is_playing = false
	_bouton_start.disabled = false
	_time_remaining = 15
	_timer_label.text = "15 s"

func _MAJ_Control_Boutons(onOff : String):
	if onOff == "Start":
		_control_boutons_joueurs.visible = false
	else :
		_control_boutons_joueurs.visible = true

func Stop_Timer(player : String):
	_timer_on = false
	_on_end_game()
	
