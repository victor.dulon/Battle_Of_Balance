extends Node2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var is_joueur : bool = true


### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _rigid_body : RigidBody2D

### INHERITED METHOD
func _ready():
	_Assigner_Variables()
	_Connect_Signals()
	_on_amelioration_done()
	pass

func _process(delta ):
	pass


### SIGNAL METHOD
func _Connect_Signals():
	EventManager._on_achat_upgrade.connect(_on_amelioration_done)

### PUBLIC METHOD


### PRIVATE METHOD
func _Assigner_Variables():
	var enfants = self.get_children()
	for enfant in enfants:
		if enfant is RigidBody2D:
			_rigid_body = enfant

func _on_amelioration_done():
	await get_tree().create_timer(0.1).timeout
	print("Amelioration murs faite ! Nouveau poids : ", RuntimeData.joueur_poids_mur)
	if is_joueur : _rigid_body.mass = RuntimeData.joueur_poids_mur
	else : _rigid_body.mass = RuntimeData.adversaire_poids_mur
