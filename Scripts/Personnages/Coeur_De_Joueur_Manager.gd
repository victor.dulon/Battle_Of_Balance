extends Node2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var is_left_player : bool = false

### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _rigid_body : RigidBody2D
var _area2D : Area2D
var _initiale_position : Vector2

### INHERITED METHOD
func _ready():
	_Assignation_Variables()
	_Connect_Signals()
	pass

### SIGNAL METHOD
func _Connect_Signals():
	_area2D.area_entered.connect(_on_dead_zone_entered)
	EventManager._on_reset_game.connect(_on_reset_game)
	pass

### PUBLIC METHOD


### PRIVATE METHOD
func _Assignation_Variables():
	var enfants = self.get_children()
	for enfant in enfants :
		if enfant is RigidBody2D:
			_rigid_body = enfant
	
	enfants = _rigid_body.get_children()
	for enfant in enfants :
		if enfant is Area2D :
			_area2D = enfant
	
	_initiale_position = self.position

func _on_dead_zone_entered(body : Area2D):
	if is_left_player :
		EventManager.InvokeOnPlayerDead("Joueur")
	else :
		EventManager.InvokeOnPlayerDead("Adversaire")

func _on_reset_game():
	_rigid_body.freeze = true
	_rigid_body.position = Vector2(0,0)
	print("rigid body position : ", _rigid_body.position)
