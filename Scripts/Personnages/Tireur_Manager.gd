extends Node2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var adversaire : bool = false
@export var tireur : Node2D

### PUBLIC VARIABLE

### PRIVATE VARIABLE

### INHERITED METHOD
func _ready():
	pass

func _process(delta ):
	pass

### INITIALISATION METHOD
##Method called for initialisation by other script to better handle initialisation order
func Init():
	_Connect_Signals()
	pass

### SIGNAL METHOD
func _Connect_Signals():
	pass

### PUBLIC METHOD
func Personnalisation():
	if !adversaire :
		tireur.cool_down = RuntimeData.joueur_cool_down
		tireur.force_du_tir = RuntimeData.joueur_force_tir
	else :
		tireur.cool_down = RuntimeData.adversaire_cool_down
		tireur.force_du_tir = RuntimeData.adversaire_force_tir

### PRIVATE METHOD

