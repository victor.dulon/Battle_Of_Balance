extends Node2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE
@export var area2D : Area2D
### PUBLIC VARIABLE

### PRIVATE VARIABLE
var _static_body : StaticBody2D
var _sprite2D: Sprite2D
var _pv : int = 3

### INHERITED METHOD
func _ready():
	_Assign_Variables()
	_Connect_Signals()
	pass


### SIGNAL METHOD
func _Connect_Signals():
	#_static_body.body_entered.connect(_on_body_entered)
	area2D.area_entered.connect(_on_body_entered)
	pass

### PUBLIC METHOD
func Recoit_Degat():
	if _pv == 3:
		_pv -= 1
		_sprite2D.modulate = Color("a3a3a3")
	elif _pv == 2:
		_pv -= 1
		_sprite2D.modulate = Color("525252")
	else :
		queue_free()
		

### PRIVATE METHOD
func _Assign_Variables():
	var enfants = self.get_children()
	for enfant in enfants:
		if enfant is StaticBody2D:
			if _static_body == null : _static_body = enfant
	
	enfants = _static_body.get_children()
	for enfant in enfants :
		if enfant is Sprite2D:
			_sprite2D = enfant

func _on_body_entered(body:RigidBody2D):
	if body.name == "Balle_RigidBody":
		body.add_constant_central_force(Vector2(1200,-600))
		self.queue_free()

