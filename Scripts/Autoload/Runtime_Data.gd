extends Node

var choix_construction_actuelle : String = "Mur"
var is_playing : bool = false

var joueur_cool_down : float
var adversaire_cool_down : float

var joueur_poids_mur : float
var adversaire_poids_mur : float

var joueur_force_tir : int
var adversaire_force_tir : int 

var joueur_money : int
var adversaire_money : int


func _ready():
	Remplissage_Variables()
	_Connections_Signaux()
	
func Remplissage_Variables():
	joueur_cool_down = ProjectData.joueur_cool_down
	adversaire_cool_down = ProjectData.adversaire_cool_down
	
	joueur_poids_mur = ProjectData.joueur_poids_mur
	adversaire_poids_mur = ProjectData.adversaire_poids_mur
	
	joueur_force_tir = ProjectData.joueur_force_tir
	adversaire_force_tir = ProjectData.adversaire_force_tir
	
	joueur_money = ProjectData.joueur_money
	adversaire_money = ProjectData.adversaire_money
	
	EventManager.InvokeOnMiseAJourArgent()

func _Connections_Signaux():
	EventManager._on_construction_choisie.connect(_on_construction_choisie)
	EventManager._on_start_game.connect(_on_off.bind(false))
	EventManager._on_end_game.connect(_on_off.bind(false))
	EventManager._on_reset_game.connect(_on_off.bind(true))
	EventManager._on_amelioration_done.connect(_on_amelioration_done)
	EventManager._on_argent_depense.connect(_on_argent_depense)

func _on_construction_choisie(type : String):
	choix_construction_actuelle = type

func _on_off(reset : bool):
	is_playing = !is_playing
	if is_playing == false:
		joueur_money += 250
		adversaire_money += 250
		EventManager.InvokeOnMiseAJourArgent()


func _on_amelioration_done(type : String, nouvelle_quantite : int):
	if type == "Cool Down Joueur":
		joueur_cool_down = nouvelle_quantite
	elif type == "Cool Down Adversaire":
		adversaire_cool_down = nouvelle_quantite
	elif type == "Force Tir Joueur":
		joueur_force_tir = nouvelle_quantite
	elif type == "Force Tir Adversaire":
		adversaire_force_tir = nouvelle_quantite
		
func _on_argent_depense(joueur : String, depense : int):
	if joueur == "Joueur":
		joueur_money -= depense
	elif joueur == "Adversaire":
		adversaire_money -= depense

func Changer_Cool_Down(joueur : String):
	if joueur == "Joueur":
		if joueur_cool_down > 0.5 :
			joueur_cool_down -= 0.25
	elif joueur == "Adversaire":
		if adversaire_cool_down > 0.5:
			adversaire_cool_down -= 0.25

func Changer_Force_Tir(joueur : String):
	if joueur == "Joueur":
		joueur_force_tir += 100
	elif joueur == "Adversaire":
		adversaire_force_tir += 100

func Changer_Poids_Mur(joueur : String):
	if joueur == "Joueur":
		joueur_poids_mur += 1
	elif joueur == "Adversaire":
		adversaire_poids_mur += 1
