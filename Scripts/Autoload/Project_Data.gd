extends Node

var joueur_cool_down : float = 2
var adversaire_cool_down : float = 2

var joueur_poids_mur : float = 5
var adversaire_poids_mur : float = 5

var joueur_force_tir : int = 1500
var adversaire_force_tir : int = 1500

var joueur_money : int = 500
var adversaire_money : int = 500
