extends Node2D

##Replace with Description

### ENUM

### SIGNAL

### EXPORT VARIABLE

### PUBLIC VARIABLE

### PRIVATE VARIABLE
#@onready var _mur : PackedScene = load(Bibliotheque.mur_path)
var _mouse_position
var _is_cooling_down : bool = false
var _is_active : bool = true
var _rotation_a_venir : int = 0
var _tween : Tween

### INHERITED METHOD
func _ready():
	_Connect_Signals()
	pass

func _process(delta : float):
	pass

func _input(event):
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_RIGHT and event.pressed:
		if !_is_cooling_down and _is_active:
			_mouse_position = get_global_mouse_position()
			
			var instance = _Chose_Prefab().instantiate()
			
			instance.position = _mouse_position
			instance.rotation_degrees = _rotation_a_venir
			if Possede_Assez_DArgent() == false : return
			EventManager.InvokeOnMiseAJourArgent()
			# Ajouter l'instance à la scène
			add_child(instance)
			_tween = create_tween()
			_tween.tween_property(instance, "scale", Vector2(1.5,1.5), 0.1)
			_tween.tween_property(instance, "scale", Vector2(1,1), 0.1)
			_is_cooling_down = true
			_ResetCoolDown(1)
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_WHEEL_UP:
		Changer_Rotation_A_Venir(true)
	if event is InputEventMouseButton and event.button_index == MOUSE_BUTTON_WHEEL_DOWN:
		Changer_Rotation_A_Venir(false)
		
### SIGNAL METHOD
func _Connect_Signals():
	EventManager._on_construction_choisie.connect(_on_construction_choisie)
	EventManager._on_start_game.connect(_Switch_On_Off.bind("Play"))
	EventManager._on_end_game.connect(_Switch_On_Off.bind("End"))
	pass

### PUBLIC METHOD
func Possede_Assez_DArgent()-> bool:
	if RuntimeData.choix_construction_actuelle == "Mur_Joueur":
		if RuntimeData.joueur_money >= 50:
			EventManager.InvokeOnArgentDepense("Joueur", 50)
			return true
		else : return false
	elif RuntimeData.choix_construction_actuelle == "Tireur_Joueur":
		if RuntimeData.joueur_money >= 100:
			EventManager.InvokeOnArgentDepense("Joueur", 100)
			return true
		else : return false
	if RuntimeData.choix_construction_actuelle == "Mur_Adversaire":
		if RuntimeData.adversaire_money >= 50:
			EventManager.InvokeOnArgentDepense("Adversaire", 50)
			return true
		else : return false
	elif RuntimeData.choix_construction_actuelle == "Tireur_Adversaire":
		if RuntimeData.adversaire_money >= 100:
			EventManager.InvokeOnArgentDepense("Adversaire", 100)
			return true
		else : return false
	elif RuntimeData.choix_construction_actuelle == "Miroir_Joueur":
		if RuntimeData.joueur_money >= 100:
			EventManager.InvokeOnArgentDepense("Joueur", 100)
			return true
		else : return false
	elif RuntimeData.choix_construction_actuelle == "Miroir_Adversaire":
		if RuntimeData.adversaire_money >= 100:
			EventManager.InvokeOnArgentDepense("Adversaire", 100)
			return true
		else : return false
	else : return false

func Clean_Enfants():
	var enfants = get_children()
	for enfant in enfants:
		enfant.queue_free()

func Changer_Rotation_A_Venir(positif : bool):
	var rotation : int
	if RuntimeData.choix_construction_actuelle == "Mur_Joueur" or RuntimeData.choix_construction_actuelle == "Mur_Adversaire":
		rotation = 45
	elif RuntimeData.choix_construction_actuelle == "Tireur_Joueur" or RuntimeData.choix_construction_actuelle == "Tireur_Adversaire":
		rotation = 45
	elif RuntimeData.choix_construction_actuelle == "Miroir_Joueur" or RuntimeData.choix_construction_actuelle == "Miroir_Adversaire":
		rotation = 10
	
	if !positif : rotation = -rotation
	

	_rotation_a_venir += rotation

### PRIVATE METHOD
func _ResetCoolDown(duree : float):
	await get_tree().create_timer(duree).timeout
	_is_cooling_down = false

func _on_construction_choisie(type : String):
	_rotation_a_venir = 0
	print("Construction choisie : ", type)

func _Chose_Prefab()-> PackedScene:
	var _instance : PackedScene
	print("Rintime Construction : ", RuntimeData.choix_construction_actuelle)
	if RuntimeData.choix_construction_actuelle == "Mur_Joueur":
		return load(Bibliotheque.mur_path)
	elif RuntimeData.choix_construction_actuelle == "Tireur_Joueur":
		return load(Bibliotheque.tireur_path)
	elif RuntimeData.choix_construction_actuelle == "Mur_Adversaire":
		return load(Bibliotheque.mur_adversaire_path)
	elif RuntimeData.choix_construction_actuelle == "Tireur_Adversaire":
		return load(Bibliotheque.tireur_adversaire_path)
	elif RuntimeData.choix_construction_actuelle == "Miroir_Joueur":
		return load(Bibliotheque.miroir_adversaire_path)
	elif RuntimeData.choix_construction_actuelle == "Miroir_Adversaire":
		return load(Bibliotheque.miroir_adversaire_path)
	else :
		printerr("Mauvaise assignation de RuntimeData.choix_construction_actuelle : ", RuntimeData.choix_construction_actuelle)
		return null

func _Switch_On_Off(onOff : String):
	if onOff == "Play":
		_is_active = false
	elif onOff == "End":
		_is_active = true

