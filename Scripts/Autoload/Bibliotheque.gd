extends Node

var mur_path : String = "res://Assets/Prefabs/Constructions/Mur.tscn"
var tireur_path : String = "res://Assets/Prefabs/Constructions/Tireur.tscn"
var mur_adversaire_path : String = "res://Assets/Prefabs/Constructions/Mur_Adversaire.tscn"
var tireur_adversaire_path : String = "res://Assets/Prefabs/Constructions/Tireur_Adversaire.tscn"
var balle_classique_path : String = "res://Assets/Prefabs/Balles/Balle_Classique.tscn"
var miroir_adversaire_path : String = "res://Assets/Prefabs/Constructions/Miroir.tscn"

var mur_prefab : PackedScene = load(mur_path)
var tireur_prefab : PackedScene = load(tireur_path)
var balle_classique_prefab : PackedScene = load(balle_classique_path)

var mur_joueur_texture : CompressedTexture2D = load("res://Assets/Sprites/Mur/Mur.png")
var mur_adversaire_texture : CompressedTexture2D = load("res://Assets/Sprites/Mur/Mur_Adversaire.png")
var tireur_joueur_texture : CompressedTexture2D = load("res://Assets/Sprites/Tireur/Tireur_Preview.png")
var tireur_adversaire_texture : CompressedTexture2D = load("res://Assets/Sprites/Tireur/Tireur_Corps_Adversaire.png")
var miroir_joueur_texture : CompressedTexture2D = load("res://Assets/Sprites/Miroir/Miroir.png")

