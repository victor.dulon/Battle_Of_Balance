extends Node

signal _on_construction_choisie(type : String)
func InvokeOnConstrutionChoisie(type : String):
	_on_construction_choisie.emit(type)

signal _on_start_game()
func InvokeOnStartGame():
	_on_start_game.emit()
	
signal _on_end_game()
func InvokeOnEndGame():
	_on_end_game.emit()

signal _on_reset_game()
func InvokeOnResetGame():
	_on_reset_game.emit()

signal _on_amelioration_done(type : String, nouvelle_quantite : int)
func InvokeOnAmeliorationDone(type : String, nouvelle_quantite : int):
	_on_amelioration_done.emit(type, nouvelle_quantite)

signal _on_argent_depense(joueur : String, depense : int)
func InvokeOnArgentDepense(joueur : String, depense : int):
	_on_argent_depense.emit(joueur, depense)

signal _mise_a_jour_argent()
func InvokeOnMiseAJourArgent():
	_mise_a_jour_argent.emit()

signal _on_achat_upgrade()
func InvokeOnAchatUpgrade():
	_on_achat_upgrade.emit()

signal _on_player_dead(player : String)
func InvokeOnPlayerDead(player : String):
	_on_player_dead.emit(player)
